class Tab {
  constructor(tabInfo) {
    this.tabInfo = tabInfo;
  }
  render() {
     this.tabInfo.map(el => {
      let elem = document.createElement("div");
      elem.className = "tab__item";
      elem.innerText = `${el.title}`
      elem.tabIndex = 0;
      document.getElementById('tab').appendChild(elem)
    }) 
}
  getData (title) {
    for(let el of this.tabInfo) {
    if(title === el.title) {
      return el.data
    }
  }


}
}
// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
tab.render()

document.querySelectorAll(".tab__item").forEach(el => el.addEventListener("click", () => makeActive(el)))
document.querySelectorAll(".tab__item").forEach(el => el.addEventListener("Enter", () => makeActive(el)))

function makeActive (el) {
  document.querySelectorAll(".tab__item").forEach(el => el.classList.remove("_current"))
  el.classList.add("_current")
  let data = tab.getData(el.innerText)
  document.getElementById('dataTab').innerText = data;
  el.style.borderTop = "none"
  el.style.borderLeft = "none"
  el.style.borderRight = "none"
}

document.addEventListener("Tab", () => {
  let curElement = document.activeElement;
  console.log(curElement)
  activeElement.addEventListener("Enter", () => makeActive(curElement))
});